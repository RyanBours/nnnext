<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePosts extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable(false)->unsigned();
            $table->string('title');
            $table->string('thumbnail')->nullable();
            $table->text('description'); // TODO: change to not required and give defaulth value of 'no description'
            $table->json('body');
            // tags list
            // group // create goups_table
            // $table-boolean('is_staged')->default($value);
            $table->timestamps();
        });

        // contraints
        Schema::table('posts', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('posts');
    }
}
