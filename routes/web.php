<?php

use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// login/registration
Auth::routes();

// index
Route::get('/', 'PostsController@index')->name('home');

// about
Route::get('/about', 'MiscController@about');

// profile
Route::get('/profile/edit', 'ProfileController@edit')->middleware('auth');
Route::get('/profile/{id}', 'ProfileController@profile')->name('profile');
Route::get('/profile', function() {
	return redirect()->route('profile', ['id' => Auth::id()]);
})->middleware('auth');

// posts controller
Route::redirect('/post', '/');

// Post Create
Route::get('/post/create', 'PostsController@create')->middleware('auth', 'role:1');
Route::post('/post/create', 'PostsController@post')->middleware('auth', 'role:1');

Route::post('/post/{id}/tag/{tag_id}/remove', 'PostsController@removeTag');
Route::post('/post/{id}/tag', 'PostsController@addTag');
Route::get('/post/{id}', 'PostsController@show');

// Post Edit
Route::get('/post/{id}/edit', 'PostsController@edit')->middleware('auth', 'role:1');
Route::post('/post/{id}/edit', 'PostsController@update')->middleware('auth');

// Post Delete
Route::delete('/post/{id}/delete', 'PostsController@delete')->middleware('auth');

// Search controller
Route::get('/search', 'SearchController@index')->name('search');
