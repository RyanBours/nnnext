<?php

if (!function_exists('Validate')) {

	/**
	 * Validate some data.
	 *
	 * @param string|array $fields
	 * @param string|array $rules
	 * @return bool
	 */
	function Validate($fields, $rules) {
	    if (! is_array($fields)) {
	        $fields = ['default' => $fields];
	    }

	    if (! is_array($rules)) {
	        $rules = ['default' => $rules];
	    }

	    return Validator::make($fields, $rules)->passes();
	}
}
