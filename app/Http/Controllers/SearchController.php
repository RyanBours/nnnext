<?php

namespace App\Http\Controllers;

use App\Post;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class SearchController extends Controller {

	public function index(Request $request) {

		$query = $request->get('query');
		$tag = $request->get('tag');

		// tags for the form
		$tags = DB::table('tags')
			->orderBy('name')
			->get();

		$result = Post::where('title', 'like', '%'.$query.'%');

		if ($tag) $result = $result->whereHas('tags',
			function($q) use ($tag) {
				$q->where('id', $tag);
			}
		);
		
		$result = $result->orderBy('created_at')
			->paginate(5);
			
		return view('search', compact('result', 'tags', 'query'));
	}

}
