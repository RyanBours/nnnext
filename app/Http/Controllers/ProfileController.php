<?php

namespace App\Http\Controllers;

use App\User;
use App\Post;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class ProfileController extends Controller {   

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile($id) {
        $user = User::find($id);

        if(!$user) return redirect('/'); // TODO: Redirect to cant find this profile page

        $posts = Post::where('user_id', $user->id)
            ->orderBy('created_at')
            ->paginate(5);

        return view('profile.index', compact('user', 'posts'));
    }

    public function edit() {
        return view('profile.edit');
    }
}
