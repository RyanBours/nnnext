<?php

namespace App\Http\Controllers;

use App\Post;
use App\Tag;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class PostsController extends Controller {

    // GET
	public function index() {
        // MAIN INDEX

        $posts = DB::table('posts')
            ->orderBy('created_at')
            ->paginate(12);

		return view('post.index', compact('posts'));
	}

    // GET
    public function show($id) {

    	$post = Post::find($id);

        $tag_names = Tag::all()->pluck('name');

        // TODO: redirect to cant find
    	if(!$post) return redirect('post/');

    	return view('post.show', compact('post', 'tag_names'));
    }

    // POST
    public function addTag(Request $request, $id) {
        $post = Post::find($id); // post
        $tag_req = request('tag'); // requested tag
        $exists = Tag::where('name', '=', $tag_req)->exists(); // find tag

        if ($exists) {
            $tag = Tag::where('name', '=', $tag_req)->first();
            if (!$post->tags->contains($tag))
                $post->tags()->attach($tag->id);
        } else {
            $tag_new = Tag::create(['name' => $tag_req]);
            $post->tags()->attach($tag_new->id);
        }

        return redirect('post/'.$id);
    }

    // POST
    public function removeTag(Request $request, $id, $tag_id) {
        $post = Post::find($id); // post
        $post->tags()->detach($tag_id);
        if (count(Tag::find($tag_id)->posts()->get()) == 0) {
            Tag::find($tag_id)->delete();
        }        
        return redirect('post/'.$id);
    }

    // GET
    public function create() {
    	
    	return view('post.create');
    }

    // GET
    public function edit(Post $post, $id) {

        $post = $post->find($id);

        if (Auth::id() != $post->user_id) return redirect('/');

    	return view('post.edit', compact('post'));
    }

    // UPDATE
    public function update(Request $request, $id) {

        // TODO: validation error messages

        // dd($request);

        $request->validate([
            'title' => 'required|max:255',
            'thumbnail' => [
                function($attribute, $value, $fail) {
                    if (gettype($value) == 'object')
                        if (!Validate($value, 'image')) return $fail($attribute.' is invalid.');
                }
            ],
            'description' => 'required|max:255',
            'post' => 'required|min:3',
            'post.*.text' => 'required',
            'post.*.img' => [
                function($attribute, $value, $fail) {
                    if (gettype($value) == 'object')
                        if (!Validate($value, 'image')) return $fail($attribute.' is invalid.');
                }
            ]
        ]);

        if(gettype($request->thumbnail) == 'object') {
            $request->validate([
                'thumbnail' => 'image|max:200000'
            ]);
        }

        $post = Post::find($id)->updatePost();

        // store optional thumbnail
        $thumbnail = $request->file('thumbnail');
        if ($thumbnail) $thumbnail->storeAs('public/'.$id, $thumbnail->getClientOriginalName());

        // store images
        if ($request->file('post') != null) foreach ($request->file('post') as $file) {
            $file['img']->storeAs('public/'.$id, $file['img']->getClientOriginalName());
        }

        return redirect('post/'.$id);
    }

    // POST
    public function post(Request $request, Post $post) {
        
        // TODO: validation error messages
        // TODO: add validation rule 'dimensions:min_width=100,min_height=200'

        $request->validate([
            'title' => 'required|max:255',
            'thumbnail' => 'image',
            'description' => 'required|max:255',
            'post' => 'required|size:3',
            'post.*.img' => 'required|image|max:200000',
            'post.*.text' => 'required'
        ]);

        // store json
        $id = $post->createPost();

        // store optional thumbnail
        $thumbnail = $request->file('thumbnail');
        if ($thumbnail) $thumbnail->storeAs('public/'.$id, $thumbnail->getClientOriginalName());

        // store post images
        foreach ($request->file('post') as $file) {
            $file['img']->storeAs('public/'.$id, $file['img']->getClientOriginalName());
        }


        // succesfull creation
        return redirect('post/'.$id);
    }

    // DELETE
    public function delete($id) {

        $post = Post::find($id);

        $post->delete(); // delete table
        // TODO: delete image files

        session()->flash('status', $post->title.' Has been deleted');

        return redirect('profile');
    }

}
