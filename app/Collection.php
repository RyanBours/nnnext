<?php

namespace App;

use App\Post;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model {

    protected $fillable = [
        'title',
    ];

    public function posts() {
        return $this->belongsToMany(Post::class);
    }

}