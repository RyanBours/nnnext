<?php

namespace App;

use App\User;
use App\Tag;
use App\Collection;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;


class Post extends Model {
    
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'body', 'user_id', 'thumbnail', 'description', 
    ];

	public function createPost() {

        $body = request('post');
        for ($i=0; $i < sizeof($body); $i++) { 
            $name = $body[$i]['img']->getClientOriginalName(); 
            $body[$i]['img'] = $name;
        }

		return Post::create([
            'user_id' => Auth::id(),
            'title' => request('title'),
            'body' => json_encode($body),
            'thumbnail' => request('thumbnail') ? request('thumbnail')->getClientOriginalName() : null,
            'description' => request('description'),
        ])->id;
	}

    public function body() {
        // return json as array
        return json_decode($this->body, true);
    }

	public function user() {
		return $this->belongsTo(User::class);
	}

    /**
     * The tags that belong to the post.
     */
    public function tags() {
        return $this->belongsToMany(Tag::class);
    }

    /**
     * The lists that belong to the post.
     */
    public function collections() {
        return $this->belongsToMany(Collection::class);
    }

    public function updatePost() {

        $body = Request('post');

        for ($i=0; $i < sizeof($body); $i++) { 
            if (gettype($body[$i]['img']) != 'string') {
                $name = $body[$i]['img']->getClientOriginalName(); 
                $body[$i]['img'] = $name;
            }
        }

        $thumb_req = request('thumbnail');
        if ($thumb_req) {
            if (gettype($thumb_req) == 'string') $thumbnail = $thumb_req;
            else $thumbnail = $thumb_req->getClientOriginalName();
        } else $thumbnail = null;

        $this->update([
            'title' => request('title'),
            'body' => json_encode($body),
            'thumbnail' => $thumbnail,
            'description' => request('description'),
        ]);

        return $this;
    }

}
