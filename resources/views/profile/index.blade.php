@extends('layouts.app')

{{-- TODO: Rename to dashboard ?? --}} 

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="modal-dialog cascading-modal modal-avatar modal-sm" role="document">
                <!--Content-->
                <div class="modal-content">

                    <!--Header-->
                    <div class="modal-header">
                        <img src="https://api.adorable.io/avatars/128/nextnextnext.click-{{ $user->name }}.png" style="min-width: 128px; height: 128px; width: 128px; height: 128px;" alt="avatar" class="rounded-circle img-responsive">
                    </div>
                    <!--Body-->
                    <div class="modal-body text-center mb-1">

                        <h5 class="mt-1 mb-2" style="font-weight: bold;">{{ $user->name }}</h5>
                        <hr />
                        <div class="md-form ml-0 mr-0">
                            <p>Posts: {{ count($user->posts) }}</p>
                            <p>info</p>
                            <p>info</p>
                            <p>info</p>
                            <p>info</p>
                        </div>

                        <div class="text-center mt-4">
                            @if($user == Auth::user())
                            <a href="/profile/edit" class="btn btn-lg btn-block orangered mt-auto">Edit Profile</a>
                            @endif
                        </div>
                    </div>

                </div>
                <!--/.Content-->
            </div>
        </div>
        <div class="col-md-8">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            <h3>Latest Posts</h3>
            <hr />
            <div class="container">
                <div class="row justify-content-center">
                    @foreach($posts as $post)
                    {{-- Post card --}}
                    <div class="card" style="margin: 1em;">
                        <!--Card image-->
                        <div class="view overlay shadow" style="transform: scale3d(1.03, 1.1, 1);">
                            <img src="{{url('/storage/'.$post->id.'/'. ($post->thumbnail ? $post->thumbnail : json_decode($post->body, true)[0]['img']))}}" class="card-img-top" alt="">
                            <a href="{{ url('post/'. $post->id) }}">
                                <div class="mask rgba-white-slight"></div>
                            </a>
                        </div>

                        <!--Card content-->
                        <div class="card-body">
                            <!--Title-->
                            <h4 class="card-title">{{ $post->title }}</h4>
                            <!--Text-->
                            <p class="card-text">{{ $post->description }}</p>
                        </div>

                        <div class="card-footer" align="right">
                            <div style="display: inline-block;">
                                <p class="card-text">Aantal slides: {{ sizeof(json_decode($post->body, true)) }}</p>

                                <a href="{{ url('post/'. $post->id) }}" class="black-text d-flex justify-content-end">
                                    <h5>Read
                                        <i class="fa fa-angle-double-right"></i>
                                    </h5>
                                </a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <nav class="d-flex justify-content-center">
                {{ $posts->links() }}
            </nav>
            @unless (count($posts))
            <h1>{{ __('Oh, it looks like this user has not created a tutorial yet!.') }}</h1> 
            @endunless
        </div>
    </div>
</div>
@endsection