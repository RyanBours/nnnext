@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-body">

                    <form method="POST" action="{{ route('register') }}">

                        @csrf

                        <p class="h4 text-center py-4">{{ __('Sign up') }}</p>

                        <!-- name -->
                        <div class="md-form">
                            <i class="fa fa-user prefix grey-text"></i>
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                            <label data-error="wrong" data-success="right" for="name">{{ __('Name') }}</label>
                            @if ($errors->has('name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <!-- email -->
                        <div class="md-form">
                            <i class="fa fa-envelope prefix grey-text"></i>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                            <label data-error="wrong" data-success="right" for="email">{{ __('E-Mail Address') }}</label>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <!-- password -->
                        <div class="md-form">
                            <i class="fa fa-lock prefix grey-text"></i>
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                            <label data-error="wrong" data-success="right" for="password">{{ __('Your password') }}</label>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="md-form">
                            <i class="fa fa-exclamation-triangle prefix grey-text"></i>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            <label data-error="wrong" data-success="right" for="password-confirm">{{ __('Password Confirmation') }}</label>
                        </div>

                        <div class="text-center py-4 mt-3">
                            <button type="submit" class="btn peach-grad">
                                {{ __('Register') }}
                            </button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
