@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                    <div class="card-body">

                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf

                            <p class="h4 text-center py-4">{{ __('Reset Password') }}</p>

                            <!-- email -->
                            <div class="md-form">
                                <i class="fa fa-envelope prefix grey-text"></i>
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                                <label data-error="wrong" data-success="right" for="email">{{ __('Your email') }}</label>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="text-center py-4 mt-3">
                                <button class="btn peach-grad" type="submit">{{ __('Send Password Reset Link') }}</button>
                            </div>
                        </form>

                    </div>

            </div>
        </div>
    </div>
</div>
@endsection
