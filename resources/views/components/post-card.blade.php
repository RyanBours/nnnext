{{-- Post card --}}
<div class="card mb-4">
	<!--Card image-->
	<div class="view overlay shadow" style="transform: scale3d(1.03, 1.1, 1);">
	    <img src="{{url('/storage/'.$post->id.'/'. ($post->thumbnail ? $post->thumbnail : json_decode($post->body, true)[0]['img']))}}" class="card-img-top" alt="">
	    <a href="{{ url('post/'. $post->id) }}">
	        <div class="mask rgba-white-slight"></div>
	    </a>
	</div>

	<!--Card content-->
	<div class="card-body">
	    <!--Title-->
	    <h4 class="card-title">{{ $post->title }}</h4>
	    <!--Text-->
	    <p class="card-text">{{ $post->description }}</p>
	</div>

	<div class="card-footer" align="right">
	  	<div style="display: inline-block;">
	  		<p class="card-text">Aantal slides: {{ sizeof(json_decode($post->body, true)) }}</p>
	  		
	    	<a href="{{ url('post/'. $post->id) }}" class="black-text d-flex justify-content-end">
	    		<h5>Read
	    			<i class="fa fa-angle-double-right"></i>
	    		</h5>
	    	</a>
	    	
		</div>
	</div>
</div>