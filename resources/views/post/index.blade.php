@extends('layouts.app')

{{-- TODO: move index.blade.php in views root ?? --}}
{{-- TODO: justify content center/start hybrid --}}

@section('content')
<section>
	{{-- carousel: most seen --}}
</section>

<section>
	{{-- scrollable lists --}}
	{{-- scrollable lists --}}
	{{-- scrollable lists --}}
</section>

<section>
	<div class="row justify-content-center">
		<div class="row justify-content-center col-10">
			@foreach ($posts as $post)
			<div class="card-deck col-lg-4">
				@postcard(compact('post'));@endpostcard
			</div>
			@endforeach
		</div>
	</div>
	<nav class="d-flex justify-content-center">
		{{ $posts->links() }}
	</nav>
</section>
@endsection
