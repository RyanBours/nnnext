@extends('layouts.app')

@push('script')
<script type="text/javascript" src="/js/post-form.js"></script>
<script type="text/javascript">
    window.onload = function() {
        addField();
        addField();
        addField();
        resetData();
    };
</script>
@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1>Create Post</h1>

            {{-- TODO: collapsable parts --}}
            {{-- TODO: moveable order --}}
            {{-- TODO: image preview --}}
            {{-- TODO: file drag&drop --}}
            {{-- TODO: add wysiwyg --}}
            {{-- TODO: display number --}}
            {{-- TODO: initial 3 --}}
            {{-- TODO: fill fields on error --}}

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div id='readroot' class="item" style="display: none">
                <div class="form-group">
                    <button class="btn btn-danger" type="button" onclick="this.parentNode.parentNode.remove(this); resetData()">{{ __('Remove') }}</button>
                </div>
                <div class="form-group">
                    <label>{{ __('Image') }}</label>
                    <input class="form-control" type="file" name="img" required />
                </div>
                <div class="form-group">
                    <textarea class="form-control" name="text" placeholder="{{ __('Text') }} ({{ __('required') }})" rows="2" cols="20" required></textarea>
                </div>  
                <hr />
            </div>

            <form method="POST" action="/post/create" enctype='multipart/form-data'>
                @csrf

                <div class="form-group">
                    <input class="form-control" type="text" name="title" placeholder="Title">
                </div>

                <div class="form-group">
                    <label>{{ __('Thumbnail (Optional)') }}</label>
                    <input class="form-control" type="file" name="thumbnail"/>
                </div>

                <div class="form-group">
                    <textarea class="form-control" name="description" placeholder="{{ __('Description') }} ({{ __('required') }})" rows="2" cols="20" required></textarea>
                </div>

                <hr />
                
                <span id="writeroot"></span>
                
                {{-- TODO: capcha --}}

                <div class="form-group">
                    <button class="btn btn-secondary" type="button" onclick="addField(); resetData()">Add</button>
                </div>
                <button class="btn btn-primary" type="submit">{{ __('Submit Post') }}</button>
            </form>
        </div>
    </div>
</div>
@endsection
