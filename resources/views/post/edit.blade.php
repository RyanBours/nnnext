@extends('layouts.app')

@push('script')
<script type="text/javascript" src="/js/post-form.js"></script>
@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1>Edit Post</h1>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div id='readroot' class="item" style="display: none">
                <div class="form-group">
                    <button class="btn btn-danger" type="button" onclick="this.parentNode.parentNode.remove(this); resetData()">{{ __('Remove') }}</button>
                </div>
                <div class="form-group">
                    <input class="form-control" type="file" name="img" required/>
                </div>
                <div class="form-group">
                    <textarea class="form-control" name="text" placeholder="Text" rows="2" cols="20" required></textarea>
                </div>  
                <hr />
            </div>

            <form method="POST" action="/post/{{$post->id}}/edit" enctype='multipart/form-data'>
                @csrf
                <div class="form-group">
                    <input class="form-control" type="text" name="title" placeholder="Title" value="{{ $post->title }}">
                </div>
                
                <div class="form-group">
                    <label>{{ __('Thumbnail (Optional)') }}</label>
                    <input class="form-control" type="file" name="thumbnail"/>
                </div>

                <input type="hidden" name="thumbnail" value="{{ $post->thumbnail }}">
                {{-- TODO: button remove thumbnail clear value of the thumbnail --}}
                
                <div class="form-group">
                    <textarea class="form-control" name="description" placeholder="{{ __('Description') }} ({{ __('required') }})" rows="2" cols="20" required>{{ $post->description }}</textarea>
                </div>

                <hr />
                {{-- <input type="hidden" name="old-post" value="{{ $post }}"> --}}
                @foreach($post->body() as $item)
                	<div id='item' class="item" style="display: block">
	                    <div class="form-group">
	                        <button class="btn btn-danger" type="button" onclick="this.parentNode.parentNode.remove(this); resetData()">{{ __('Remove') }}</button>
	                    </div>

	                    <div class="form-group">
	                        <input class="form-control" type="file" name="post[{{ $loop->index }}][img]"/>
	                    </div>
	                    {{-- TODO: button to reset the new images --}}

	                    <input type="hidden" name="post[{{ $loop->index }}][img]" value="{{ $item['img'] }}">

	                    <div class="form-group">
	                    	<img src="{{ url('/storage/'.$post->id.'/'.$post->body()[$loop->index]['img']) }}" style="max-width: 200px;max-height: 200px;">
	                    </div>

	                    <div class="form-group">
	                        <textarea class="form-control" name="post[{{ $loop->index }}][text]" placeholder="{{ __('Text') }} ({{ __('required') }})" rows="2" cols="20" required>{{$item['text']}}</textarea>
	                    </div>

	                    <hr />
	                </div>
                @endforeach

                <span id="writeroot"></span>
                <div class="form-group">
                    <button class="btn btn-secondary" type="button" onclick="addField(); resetData()">Add</button>
                </div>
                <button class="btn btn-primary" type="submit">Submit Post</button>
            </form>
        </div>
    </div>
</div>
@endsection
