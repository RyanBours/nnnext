@extends('layouts.app')

@push('css')
<style type="text/css">
    .carousel-control-next:hover, .carousel-control-prev:hover {
        background-color: grey;
        opacity: 0.3;
    }
</style>
@endpush

@push('script')
<script type="text/javascript">
    let tags = {!! json_encode($tag_names->toArray()) !!};
    window.onload = function() {
        autocomplete(document.getElementById("tagger"), tags);   
    };
</script>
@endpush

@section('content')
<div class="row justify-content-md-center">
    <div class="col-lg-10" style="background-color:#EEE; box-shadow: 0 5px 11px 0 rgba(0,0,0,.18), 0 4px 15px 0 rgba(0,0,0,.15); outline: 0; padding: 0;">

        {{-- <div>
        
        </div> --}}

        <!-- Carousel -->
        <div id="carousel-nnnext" class="carousel slide" data-ride="carousel" data-wrap="false" data-interval="false">

            <!-- Slides -->
            <div class="carousel-inner" role="listbox">
                @foreach ($post->body() as $item)
                <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                    <div class="view" style="padding: 0 0"> 
                        <img class="d-block w-100" style="height:24em; width:auto; object-fit:contain; background-color:#333" src="{{ url('/storage/'.$post->id.'/'.$item['img']) }}" alt="slide {{ $loop->index }}">
                    </div>
                    <div class="card-footer" style="min-height:24em; background-color:#FFF">
                        <h3 class="h3-responsive">Lorem Ipsum</h3>
                        <p>{{ $item['text'] }}</p>
                    </div>
                </div>
                @endforeach
            </div>
            <!--Slides -->

            <!-- Controls -->
            <a class="carousel-control-prev" href="#carousel-nnnext" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-nnnext" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
            <!-- /Controls -->

        </div>
        <!-- /Carousel -->

        <div class="d-flex justify-content">
            <div class="mr-auto">
                @if($post->user == Auth::user())
                <a class="btn btn-danger" href="" onclick="event.preventDefault(); if(confirm('You\'re about to delete your post')) document.getElementById('delete-form').submit();">
                    {{ __('Delete') }}
                </a>
                    
                <a class="btn btn-warning" href="{{ url()->current().'/edit' }}">
                    {{ __('Edit') }}
                </a>
                        
                <form id="delete-form" action="{{ url('post/'.$post->id.'/delete') }}" method="post" style="display:none;">
                    @method('DELETE')
                    @csrf
                </form>
                @endif
            </div>
            <div class="mr-auto ml-auto d-flex align-items-center">
                @if($post->user == Auth::user())
                <form method="POST" action="/post/{{ $post->id }}/tag" autocomplete="off">
                    @csrf
                    <div class="autocomplete" style="width:8em;padding: 0">
                        <input id="tagger" type="text" class="form-control" style="border-radius: 0;" name="tag" placeholder="Add Tag" required>
                    </div>
                    <button class="btn btn-warning" style="padding:1em; transform: none; margin: 0;" type="submit">
                        <i class="fas fa-plus"></i>
                    </button>
                </form>
                @endif
            </div>
            <div class="ml-auto d-flex align-items-center mt-auto mb-auto mr-3">
                
                @foreach ($post->tags as $tag)
                <div style="font-size: 22px;">
                    <a href="/search?tag={{ $tag->id }}" class="badge rad-grad" style="margin-left: 1em;">{{ __($tag->name) }}</a>
                    @if($post->user == Auth::user())
                <a class="badge btn-danger" onclick="event.preventDefault(); if(confirm('You\'re about to remove a tag from your post')) document.getElementById('delete-tag{{$tag->id}}').submit();">
                        <i class="fas fa-times"></i>
                    </a>
                    <form id="delete-tag{{$tag->id}}" action="{{ url('post/'.$post->id.'/tag/'.$tag->id.'/remove/') }}" method="post" style="display:none;">
                        @csrf
                    </form>
                    @endif
                </div>
                @endforeach
            </div>
        </div>

    </div>
</div>
@endsection