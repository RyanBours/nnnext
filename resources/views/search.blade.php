@extends('layouts.app')

@push('script')
<script type="text/javascript">
    window.onload = function() {
        toggleNav('filternav');
    };
</script>
@endpush


@section('content')
{{-- Search filter --}}
<span style="position:fixed;background-color:#FFF;margin-left:-0.95em;padding:0.4em;border-radius: 0 0.8em 0.8em 0; z-index: 99; border: 1px solid rgba(0,0,0,.125);" onclick="toggleNav('filternav')"><i class="fas fa-filter"></i></span>
<div id="filternav" class="sidenav mt-5 pt-5" data-toggle='false'>
	<form style="margin: 0 1em 0 1em; height: 100%;">
		<p class="h4 text-center py-4">{{ __('Filters') }}</p>
		
		<input class="form-control" type="search" name='query' placeholder="Search" value="{{ $query }}" aria-label="Search">

		<div class="md-form">
			{{-- TODO: multi-tag selection --}}
			<select name="tag" class="custom-select">
				<option value="">Tag</option>
				@foreach ($tags as $tag)
				<option value="{{ $tag->id }}">{{ $tag->name }}</option>
				@endforeach
			</select>
		</div>

		<div class="text-center py-4 mt-3">
			<button class="btn peach-grad" type="submit">
				{{ __('Search') }}
			</button>
		</div>

	</form>
</div>
{{-- /Search filter --}}

{{-- Search result --}}
<div class="row justify-content-center">
	<div class="col-md-10">
		<div class="list-group">
			@foreach ($result as $post)	
			<div class="text-right align-middle list-group-item" style="width: 100%; margin-bottom: 0.1em; border-radius: 0;">
				<div class="row">
					<div class="col-md-8">
						<div class="view overlay">
							<img class="img-fluid" src="{{ url('/storage/'.$post->id.'/'. ($post->thumbnail ? $post->thumbnail : json_decode($post->body, true)[0]['img'])) }}" class="card-img-top" alt="">
							<a href="{{ url('post/'. $post->id) }}">
								<div class="mask rgba-white-slight"></div>
							</a>
						</div>
					</div>
					<div class="col-md-4" style="border-left: 1px solid rgba(0,0,0,.125); border-right: 1px solid rgba(0,0,0,.125)">
						<div class="card-body d-flex flex-column align-items-start" style="height: 100%; text-align: left;">
							<h5 class="card-title">{{ $post->title }}</h5>
							<p class="card-text">{{ $post->description }}</p>
							
							<a href="{{ url('post/'. $post->id) }}" class="btn btn-lg btn-block orangered mt-auto">See</a>
						</div>
					</div>
				</div>
			</div>
			@endforeach

			@unless (count($result))
    			<h1>{{ __('Whoops, looks like there are no results.') }}</h1>
			@endunless
		</div>
	</div>
</div>
{{-- /Search result --}}

<nav class="d-flex justify-content-center">
	{{ $result->links() }}
</nav>

@endsection
