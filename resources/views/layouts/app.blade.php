<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- TODO: Change to '[app.name] - [page]' --}}
    <title>{{ config('app.name', 'NNNext') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">

    <!-- Styles -->
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.4/css/mdb.min.css" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @stack('css')

</head>
{{-- background-color: rgba(244, 67, 54, 0.05); --}}
{{-- linear-gradient(320deg, #FFE771, #CA513E); --}}
{{-- linear-gradient(294deg, #EF7E61, #E76053); --}}
{{-- linear-gradient(307deg, #A4EAA8, #FA7B54); --}}
{{-- linear-gradient(14deg, #ADBEA7, #DE5655); --}}
{{-- linear-gradient(254deg, #DE9BB3, #D8D162); --}}
{{-- linear-gradient(248deg, #F6DFFE, #D66347); --}}
{{-- linear-gradient(1deg, #EBC297, #EF7FB9); --}}
{{-- linear-gradient(357deg, #EA4F70, #FBF1AA); --}}
<body> 

    {{-- TODO: Add sidenav and add profile create etc. buttons --}}
    
    <div id="app" style="transition:0.3s">
        @nav;@endnav

        <main class="mt-5 pt-5">
            <div class="container-fluid">
                @yield('content')
            </div>
        </main>

        @footer;@endfooter
    </div>

    <!-- Scripts -->
    <!-- JQuery -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/umd/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.4/js/mdb.min.js"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    @stack('script')
    
</body>
</html>
