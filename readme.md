![NNNext Logo](https://bitbucket.org/RyanBours/nnnext/raw/a743e67815423ff284fcf4b56c574dbc48670fb2/public/assets/nnnext-logo3.png)
## Installation & configuration (local development)

- ```Git clone```
- ```cd nnnext```
- ```composer install```
- Create a **.env** file
	- **TIP**: (windows) Create a txt file then ```rem filetorename.txt .env```
- Paste the content of **.env.example** into **.env** and cofigure it to your needs
	- **TIP**: [mailtrap.io](mailtrap.io) can be used for testing mails (for **Free**)
	- **NOTE**: if mail host isn't set-up, it will crash at user registration 
- ```php artisan migrate:fresh```
- ```php artisan key:generate```
- ```php artisan storage:link```

You can use the built-in webserver to run the nnnext app using:
```php artisan serve```

## Links
* [PHP](http://php.net/)
* [Laravel 5](https://laravel.com/)
* [MYSQL](https://www.mysql.com/)
* [MAILTRAP](https://mailtrap.io/)
* [Composer](https://getcomposer.org/)

## TODO
- Rework post display
- Rework profile page
- Rework about page
- Add captcha to post create page
- Fix footer
- Rework mail