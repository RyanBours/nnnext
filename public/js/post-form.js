let ID = function () {
  return '_' + Math.random().toString(36).substr(2, 9);
};

function addField() {
	let clone = document.getElementById('readroot').cloneNode(true);
	clone.id = ID();
	clone.style.display = 'block';
	let nodes = clone.childNodes;
	for (let n of nodes) {
		if (n.name) n.name = "post[0][" + n.name + "]"; 
		else {
			let subnodes = n.childNodes;
			if (subnodes) for (let sn of subnodes) {
				if (sn.name) sn.name = "post[0][" + sn.name + "]";
			}
		}
	}
	let insertHere = document.getElementById('writeroot');
	insertHere.parentNode.insertBefore(clone, insertHere);
}

function resetData() {
	let resetCounter = -1;
	let pattern = /\d/g;

	let items = document.getElementById('writeroot').parentNode.childNodes;
	for (let i of items) {
		if (i.classList) if (i.classList.contains('item')) {
			resetCounter++;			
			for (let child of i.childNodes) {
				if (child.name) {
					if (child.name) child.name = child.name.replace(pattern, resetCounter);	
				} else {
					let subnodes = child.childNodes;
					if (subnodes) for (let sn of subnodes) {
						if (sn.name) sn.name = sn.name.replace(pattern, resetCounter);
					}
				}
			}
		}
	}
}